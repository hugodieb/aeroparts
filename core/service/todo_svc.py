from core.models import Todo

def add_todo(new_task):
    todo = Todo(description=new_task)
    todo.save()
    return todo.to_dict_json()


def list_todos():
    todos = Todo.objects.all()
    return [todo.to_dict_json() for todo in todos]


def task_update(params, todo_id):
    description, done = [params.get(k) for k in ('description', 'done')]
    todo = Todo.objects.get(pk=todo_id)
    todo.done = done
    todo.save()
