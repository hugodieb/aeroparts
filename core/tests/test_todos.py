from core.models import User, Todo
from django.test.client import Client
from django.test.testcases import TestCase
from core.tests import fixtures
import json
import copy


TASK = {
    'description': 'walk the dog',
    'done': False
}


class TestAuthApi(TestCase):
    @classmethod
    def setUpTestData(cls):
        fixtures.user_jon()

    def test_auth_api(self):
        client = Client()
        client.force_login(User.objects.get(username='jon'))
        r1 = client.post('/api/add_todo', {'new_task': 'walk the dog'})
        r2 = client.post('/api/add_todo', {'new_task': 'do the laundry'})
        r3 = client.get('/api/list_todos')
        self.assertEqual({200}, {r.status_code for r in [r1, r2, r3]})
        todos = json.loads(r3.content.decode('utf-8'))
        self.assertEqual(2, len(todos['todos']))



    def test_update_todo_task_completed(self):
        client = Client()
        client.force_login(User.objects.get(username='jon'))
        dotask = copy.deepcopy(TASK)
        dotask.update({'done': True})
        self._update_and_assert_todo(client, dotask)


    def _update_and_assert_todo(self, client, tododata, expecterror=False, msg=None):
        r1 = client.post('/api/add_todo', {'new_task': 'walk the dog'})
        self.assertEqual(200, r1.status_code)
        todo = Todo.objects.get(description='walk the dog')
        tododata.update({'id': todo.id})
        params = json.dumps(tododata)
        r = client.post('/api/task_update', {'params': params})
        self.assertEqual(200, r.status_code)
        res = json.loads(r.content.decode('utf-8'))
        if expecterror:
            self.assertTrue('error' in res)
            self.assertEquals(res['error'], msg)
        else:
            self.assertEquals(0, len(res))
